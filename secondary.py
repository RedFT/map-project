#!/usr/bin/env python2.7
import sqlite3
import pickle
import pprint

# Project Modules
from src.db_handler import *
from src.geometry import *
from src.drawing import *
from src.region_growing import *
from src.stitch import *
from src.constants import *
from src.utils import *


def run():
    conn = sqlite3.connect(sample_database_path)
    cursor = conn.cursor()
    rooms = get_rooms(cursor, 601, 682)
    doors = get_doors(cursor)
    conn.close()

    img = FLOOR_IMAGE.copy()
    outlines = np.zeros([h, w, 3], np.uint8)
    look_up = get_regions(img.copy(), rooms)

    with open(join_path(data_dir, "lookup_table.dat"), "w") as f:
        pickle.dump(look_up, f)

    for door in doors:
        look_up, connecting_rooms = get_connecting_rooms(img, door, look_up)

    with open(join_path(data_dir, "lookup_table_with_unknowns.dat"), "w") as f:
        pickle.dump(look_up, f)

    im_bw = make_black_and_white(REGIONS.copy())
    contours = cv2.findContours(im_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]

    # Remove Biggest Contour
    biggest_contour = 0
    for i, c in enumerate(contours[:]):
        area = cv2.contourArea(c)
        if area > cv2.contourArea(contours[i]):
            biggest_contour = i
    contours.pop(biggest_contour)

    rel_contours = []
    for c in contours:
        # Remove contours that are too small (area < 100px)
        area = cv2.contourArea(c)
        if area > 100:
            rel_contours.append(c)

    room_contour_pairs = []
    contour_graph = {}
    for i in range(0, len(rel_contours)):
        points = [x[0] for x in [x for x in rel_contours[i]]]
        new_points = []

        # straighten out diagonal lines of length sqrt(2)
        for j in range(0, len(points)):
            a = np.array(points[j])
            b = np.array(points[j - 1])
            c = np.array(points[j - 2])

            if np.sqrt(abs((b - c).dot(b - c))) == 0:
                continue
            if np.sqrt(abs((a - b).dot(a - b))) - np.sqrt(2) < 0.01:
                b = b + ((b - c) / np.sqrt(abs((b - c).dot(b - c))))
                points[j - 1] = b

        # combine co-linear points
        for j in range(0, len(points)):
            a = np.array(points[j])
            b = np.array(points[j - 1])
            c = np.array(points[j - 2])

            # check (a - b) == (a - b) + (b - c)
            pa = get_magnitude(a, b)
            pb = get_magnitude(b, c)
            pc = get_magnitude(a, c)

            if pa + pb - pc < 0.01:
                points[j - 1] = (-1, -1)  # if collinear, mark for removal

        # filter out marked points
        for point in points:
            if tuple(point) != (-1, -1):
                new_points.append(point)

        # TODO: remove contours of doors

        points = new_points  # let assign new_points to points since I used it a lot further down...
        points = map(np.array, points)

        bbox = bound_points(points, 1)

        p1, p2 = get_qualifying_wall(points)
        box_intersections = line_box_intersection((p1, p2), bbox)
        intersection_points = line_contour_intersection(box_intersections, points)
        inner_points = get_inside_points(intersection_points)

        if not inner_points:
            print "Could not find any inside points for contour #", i

        room = None
        for ip in inner_points:
            room = look_up[ip[1], ip[0]]
            if room:
                break

        if not (str(room) in contour_graph):
            contour_graph[str(room)] = points
        else:
            print "CONFLICT"

        M = cv2.moments(rel_contours[i])
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])

        draw_text(outlines, str(room), (cx - 15, cy),
                  .4, (200, 10, 10))

        room_contour_pairs.append((rel_contours[i], room))

    # good_rooms = contour_graph.keys()
    room_graph = {}
    door_graph = {}
    for door_idx, door in enumerate(doors):
        nearest_rooms = []
        box_size = 0
        while len(nearest_rooms) < 2:
            box_size += 1

            roi = look_up[door[1] - box_size:door[1] + box_size + 1, door[0] - box_size:door[0] + box_size + 1]
            roi_1d = np.reshape(roi, -1)
            unique_rooms = np.unique(roi_1d[np.nonzero(roi_1d)])

            if len(unique_rooms) > 1:
                non_zero_indices = np.nonzero(roi)

                for unique_room in unique_rooms:
                    rooms1 = [(y, x) for y, x in zip(non_zero_indices[0], non_zero_indices[1]) if
                              roi[y, x] == unique_room]
                    dists1 = [get_magnitude(room, (box_size, box_size)) for room in rooms1]
                    closest = roi[rooms1[dists1.index(min(dists1))]]
                    nearest_rooms.append((closest, min(dists1)))
                print roi
                break

        room_A, room_B = sorted(nearest_rooms, key=op.itemgetter(1))[:2]
        room_A = room_A[0]
        room_B = room_B[0]

        '''
        if not ((str(room_A) in good_rooms) and (str(room_B) in good_rooms)):
            continue
        '''

        # Generate graph
        try:
            room_list = room_graph[str(room_A)]
            if str(room_B) not in room_list:
                room_list.append(str(room_B))
        except KeyError:
            room_graph[str(room_A)] = [str(room_B)]
        try:
            room_list = room_graph[str(room_B)]
            if str(room_A) not in room_list:
                room_list.append(str(room_A))
        except KeyError:
            room_graph[str(room_B)] = [str(room_A)]

        door_graph["d" + str(door_idx)] = [str(room_A), str(room_B)]

    # mark hallways
    hallway_number_string = "2000"
    new_room_graph = {
            hallway_number_string: [],
            }
    hallways = []
    for k in room_graph.keys():
        if len(room_graph[k]) <= 6:
            continue

        hallways.append(k)
        new_room_graph[hallway_number_string] += room_graph[k]

    for k in room_graph.keys():
        if k in hallways:
            continue
        new_room_graph[k] = room_graph[k]

    for k in new_room_graph.keys():
        rooms = new_room_graph[k]
        new_rooms = []
        for room in rooms:
            if room in hallways:
                new_rooms.append(hallway_number_string)
                continue
            new_rooms.append(room)
        new_rooms = list(set(new_rooms))

        new_room_graph[k] = new_rooms


    with open(join_path(data_dir, "a_graph.dat"), "w") as f:
        pickle.dump(new_room_graph, f)

    cv2.imshow("debug", outlines)
    cv2.waitKey(0)
    cv2.imshow("debug", REGIONS)
    cv2.waitKey(0)


if __name__ == "__main__":
    toggle_drawing()
    cv2.namedWindow("debug", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("debug", 800, 600)
    run()
    cv2.destroyAllWindows()
