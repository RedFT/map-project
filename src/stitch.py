"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
import os

import cv2
import numpy as np

from region_growing import *
from constants import *
from utils import *


def get_regions(img_to_use, rooms):
    """get regions of rooms
    """
    img_bw = make_black_and_white(img_to_use.copy(), 254)
    img_bw = 255 - img_bw
    kernel = np.ones([2, 2], np.uint8)
    imgnew = cv2.erode(img_bw, kernel, iterations=1)
    imgnew= 255 - imgnew
    img_bw = imgnew

    cv2.imwrite(os.path.join(output_dir, "img_bw.png"), img_bw)

    h, w            = img_to_use.shape[:2]
    mask            = np.zeros(img_to_use.shape[:2], np.uint8)
    room_regions    = np.zeros(img_bw.shape[:2], np.uint16)

    for room in rooms:
        x, y        = (int(room[0]), int(room[1]))
        room_data   = room[2]

        room_regions, dim_area = grow_region(
                img_bw, 
                (x, y), room_regions, room_data)

        if not room_data in map_room_area:
            map_room_area[room_data] = dim_area[1]
        else:
            map_room_area[room_data] +=  dim_area[1]

    return room_regions


unknown_room_number = 1000
hallway_room_number = 2000
def get_connecting_rooms(img, door, look_up_table):
    global unknown_room_number
    global REGIONS

    mx = door[3]
    my = door[4]

    angle1 = door[5]
    angle2 = door[6]

    final_x = door[0] + mx * math.cos (angle1)
    final_y = door[1] + my * math.sin (angle2)

    final_x = int(final_x)
    final_y = int(final_y)

    d_rect = bound_points([door[:2]], 3)
    img_rect = Rect(0, 0, img.shape[1], img.shape[0])

    corner_rooms = [0, 0, 0, 0]
    corners = [d_rect.top_left, d_rect.top_right, 
            d_rect.bottom_left, d_rect.bottom_right]

    for i, corner in enumerate(corners):
        corner_rooms[i] = look_up_point(corner, look_up_table)

    connecting_rooms = []
    for i, room in enumerate(corner_rooms):
        if room != 0 and room not in [r[0] for r in connecting_rooms]:
            connecting_rooms.append((room, corners[i]))

    if len(connecting_rooms) >= 2:
        return (look_up_table, connecting_rooms)
    
    img_bw = make_black_and_white(img.copy(), 254)
    img_bw = 255 - img_bw
    kernel = np.ones([2, 2], np.uint8)
    imgnew = cv2.erode(img_bw, kernel, iterations=1)
    imgnew= 255 - imgnew
    img_bw = imgnew
    for i, corner in enumerate(corners):
        # if a room was already found, continue
        if corner_rooms[i] != 0:
            continue

        # if pixel is black, continue
        if img_bw[corner[1], corner[0]] == 0:
            continue

        if look_up_point(corner, look_up_table) != 0:
            continue

        # grow region if everything is ok
        look_up_table, dim_area = grow_region(img_bw, corner, look_up_table, unknown_room_number)

        if unknown_room_number not in [r[0] for r in connecting_rooms]:
            #area / max(w, y)
            dim, area = dim_area
            if not unknown_room_number in map_room_area:
                map_room_area[unknown_room_number] = area
            else:
                map_room_area[unknown_room_number] +=  area

            large_axis = float(max(dim.w, dim.h))
            small_axis = float(min(dim.w, dim.h))

            ratio = 0
            if large_axis != 0:
                ratio = small_axis / large_axis 

            connecting_rooms.append((
                unknown_room_number, 
                corner, 
                ratio,
                area
                ))

            unknown_room_number += 1

    return (look_up_table, connecting_rooms)
