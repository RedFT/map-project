"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
import sqlite3

from geometry import *

def get_room(cursor, item):
    cmd = "select * from text where text = \"" + item + "\";"
    cursor.execute(cmd)
    ret = cursor.fetchall()
    point = None
    for room in ret:
        if room[2] != 0:
            point = room

    if not point:
        return None

    point = point[2:4]

    point = scale_translate_point(point)
    point = (point[0], flip_y(point[1], h))
    point = [int(p) for p in point]

    return point

def get_rooms(cursor, low, high):
    rooms = range(low, high+1)
    room_coords = []
    for room in rooms:
        room_name = str(room)
        if room == 660:
            room_name = room_name+"W"
        elif room == 665 or room == 666:
            room_name = room_name+"M"
        point = get_room(cursor, room_name)
        if not point:
            continue
        room_coords.append((point[0], point[1], room))

    return room_coords

def get_doors(cursor):
    cmd = "select * from ellipses;"
    cursor.execute(cmd)
    ret = cursor.fetchall()
    doors = []
    for door in ret:
        #(cx, cy, id, mx, my, theta1, theta2)
        doors.append((int(door[3]), int(door[4]), 
            door[0], 
            door[5], door[6],
            door[1], door[2]))

    new_doors = []
    for door in doors:
        point = scale_translate_point(door[:2])
        point = (point[0], flip_y(point[1], h))
        point = [int(p) for p in point]

        axes = scale_point(door[3:5])
        axes = (axes[0], -axes[1])
        axes = [int(round(p)) for p in axes]

        new_doors.append((point[0], point[1], 
            door[2], 
            axes[0], axes[1],
            door[5], door[6]))
    return new_doors


def table_exists(cursor, table_name):
    cmd = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + \
            table_name + "';"
    cursor.execute(cmd)
    ret = cursor.fetchall()
    if ret:
        return True
    return False


def new_table(cursor, table_name):
    # id room_number connecting_doors 
    cmd = "CREATE TABLE " + table_name + "();"
