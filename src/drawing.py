"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
import cv2
from geometry import *

drawing = False
def toggle_drawing():
    global drawing
    if drawing == True:
        drawing = False
        print "Drawing is toggled off"
    else:
        drawing = True
        print "Drawing is toggled on"

def draw_point(img, point, color):
    """Draw a dot at 'point' of
    color (BGR)
    """
    global drawing
    if drawing != True:
        return

    h, w, c = img.shape
    if is_bounded(Rect(0, 0, w, h), point):
        pixel = img[point[1], point[0]]
        for i, intensity in enumerate(color):
            pixel[i] = intensity


def draw_box(img, center, half_of_side, color):
    """Draw a box around the point 'center'
    of width and length (2 * 'half_of_side')
    """
    global drawing
    if drawing != True:
        return

    cv2.rectangle(img, 
            (center[0] - half_of_side, center[1] - half_of_side),
            (center[0] + half_of_side, center[1] + half_of_side),
            color)


def draw_text(img, text, point, size, color):
    global drawing
    if drawing != True:
        return

    cv2.putText(img, text, 
            (point[0]+5, point[1]-5), 
            cv2.FONT_HERSHEY_SIMPLEX, 
            size, 
            color)


def draw_contours(img, contours):
    pass
