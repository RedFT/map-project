"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
import numpy as np
import operator as op
import math
import cv2
from constants import FLOOR_IMAGE

h, w = FLOOR_IMAGE.shape[:2]

### FLOOR 6 #####
SCALE_FACTOR_X = .3750
SCALE_FACTOR_Y = .3462

TRANSLATION_X = -1507.0
TRANSLATION_Y = -159.
#################

class Rect(object):
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

        self.left = x
        self.right = x + w
        self.top = y
        self.bottom = y + h


        self.top_left = (self.left, self.top)
        self.top_right = (self.right, self.top)
        self.bottom_left = (self.left, self.bottom)
        self.bottom_right = (self.right, self.bottom)

        self.center_x = self.left + (self.w / 2)
        self.center_y = self.top  + (self.h / 2)

        self.center_left          = (self.left, self.center_y)
        self.center_right 	  = (self.right, self.center_y)
        self.center_top           = (self.center_x, self.top)
        self.center_bottom        = (self.center_x, self.bottom)


    def __getitem__(self, key):
        if key == 0:
            return self.x
        if key == 1:
            return self.y
        if key == 2:
            return self.w
        if key == 3:
            return self.h
        if key == 4:
            return self.right
        if key == 5:
            return self.bottom

    def __repr__(self):
        return str((self.x, self.y, self.w, self.h))

    def draw(self, img, color):
        cv2.line(img, self.top_left, self.top_right, color)
        cv2.line(img, self.top_right, self.bottom_right, color)
        cv2.line(img, self.bottom_right, self.bottom_left, color)
        cv2.line(img, self.bottom_left, self.top_left, color)
        


def rotate_point(point1, point2, theta):
    """Rotate 'point1' around 'point2' 
    by angle 'theta' and return
    new coordinates
    """
    ox = point1[0] - point2[0]
    oy = point1[1] - point2[1]
    x = math.cos(theta) * ox - math.sin(theta) * oy
    y = math.sin(theta) * ox + math.cos(theta) * oy
    new_p = (x + point1[0], y + point1[1])
    return new_p


def bound_points(points, padding=0):
    """bound an array of points into a Rect.
    """
    x_cmps = [point[0] for point in points]
    y_cmps = [point[1] for point in points]

    x      = min(x_cmps) - padding
    y      = min(y_cmps) - padding
    width  = max(x_cmps) - x + padding
    height = max(y_cmps) - y + padding

    return Rect(x, y, width, height)


def is_bounded(rect, point):
    """Return True if 'point' is inside 'rect'
    """
    x, y = point[:2] 

    return (x >= rect.x and x < rect.right) and \
            (y >= rect.y and y < rect.bottom)


def is_colliding(rect1, rect2):
    """Return True if rect1 overlaps rect2
    """
    if rect1.right < rect2.left:
        return False
    if rect1.left > rect2.right:
        return False
    if rect1.bottom < rect2.top:
        return False
    if rect1.top > rect2.bottom:
        return False
    return True


def scale_up(point, factor):
    return (int(point[0] * factor), int(point[1] * factor))


def scale_point(point):
    """Scale down original coordinates
    """
    new_x = point[0] * SCALE_FACTOR_X
    new_y = point[1] * SCALE_FACTOR_Y
    return (new_x, new_y) 


def translate_point(point):
    """Translate Coordinates for smaller image
    """
    new_x = point[0] + TRANSLATION_X
    new_y = point[1] + TRANSLATION_Y 
    return (new_x, new_y)


def scale_translate_point(point):
    """Scale down and translate a point
    from the original autocad image
    """
    return translate_point(scale_point(point))


def flip_y(y, height):
    return height - y


def get_contours(img_bw):
    return cv2.findContours(img_bw.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]


<<<<<<< HEAD
=======
def get_magnitude(point1, point2):
    return math.sqrt(((point1[0] - point2[0])**2) + ((point1[1] - point2[1])**2))


>>>>>>> norbu_dev
def line_intersection(line1, line2):
    pointa, pointb = line1
    line1 = ((float(pointa[0]), float(pointa[1])), (float(pointb[0]), float(pointb[1])))
    pointa, pointb = line2
    line2 = ((float(pointa[0]), float(pointa[1])), (float(pointb[0]), float(pointb[1])))
<<<<<<< HEAD
=======


>>>>>>> norbu_dev
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def line_box_intersection(line1, bbox):
    intersections = []
    bline1 = [(bbox.x,          bbox.y), 
              (bbox.x + bbox.w, bbox.y)]
    bline2 = [(bbox.x + bbox.w, bbox.y), 
              (bbox.x + bbox.w, bbox.y + bbox.h)]
    bline3 = [(bbox.x + bbox.w, bbox.y + bbox.h), 
              (bbox.x,          bbox.y + bbox.h)]
    bline4 = [(bbox.x,          bbox.y + bbox.h), 
              (bbox.x,          bbox.y)]

    for test_line in [bline1, bline2, bline3, bline4]:
        intersection = line_intersection(line1, test_line)
        if intersection:
            intersections.append(intersection)
    return intersections


<<<<<<< HEAD
def get_magnitude(point1, point2):
    return math.sqrt(((point1[0] - point2[0])**2) + ((point1[1] - point2[1])**2))
=======
def line_contour_intersection(line, contour):
    intersection_points = []
    for j in range(1, len(contour)):
        pointa, pointb = contour[j], contour[j-1]

        intersection = line_intersection(line, (pointa, pointb))

        if not intersection:
            continue

        real_intersection = False
        #check if the intersection lies on both lines
        for lin in [line, (pointa, pointb)]:
            pa = (get_magnitude(lin[0], intersection))
            pb = (get_magnitude(lin[1], intersection))
            pc = (get_magnitude(lin[0], lin[1]))
            #print pa, pb, pc

            # if intersection is on one of the lines
            if (abs(pa + pb - pc) < .00000001):
                real_intersection = True

        if real_intersection:
            intersection_points.append(intersection)
    pointa, pointb = contour[0], contour[-1]

    intersection = line_intersection(line, (pointa, pointb))

    if not intersection:
        return intersection_points

    real_intersection = False
    #check if the intersection lies on both lines
    for lin in [line, (pointa, pointb)]:
        pa = (get_magnitude(lin[0], intersection))
        pb = (get_magnitude(lin[1], intersection))
        pc = (get_magnitude(lin[0], lin[1]))
        #print pa, pb, pc

        # if intersection is on one of the lines
        if (abs(pa + pb - pc) < .00000001):
            real_intersection = True

    if real_intersection:
        intersection_points.append(intersection)

    return intersection_points


def get_inside_points(intersection_points):
    inner_points = []
    p1 = intersection_points[0]
    p2 = intersection_points[1]

    if abs(p1[0] - p2[0]) > abs(p1[1] - p2[1]):
        intersection_points = sorted(intersection_points, key=op.itemgetter(0, 1))
    else:
        intersection_points = sorted(intersection_points, key=op.itemgetter(1, 0))

    for j in range(1, len(intersection_points)):
        if (j % 2 != 0):
            start = np.array(intersection_points[j-1])
            end   = np.array(intersection_points[j])
            mid2 = (end  - start) / 2 + start
            mid1 = (mid2 - start) / 2 + start
            mid3 = (end  - mid2)  / 2 + mid2

            inner_points += [mid1, mid2, mid3]
            #inner_points.append(mid2)

    return inner_points


def get_qualifying_wall(points):
    midpoint = 0
    slope    = 0
    p1       = 0
    p2       = 0

    lengths = [get_magnitude(points[j], points[j-1]) for j in range(1, len(points))]
    max_index = lengths.index(max(lengths))+1

    midpoint = (points[max_index] - points[max_index-1]) / 2 + points[max_index-1]
    slope = (points[max_index] - points[max_index-1])
    slope = slope / math.sqrt((slope[0] ** 2 + slope[1] ** 2))
    slope *= 1000
    p1 = [int(x) for x in (midpoint[0] + slope[1], midpoint[1] + slope[0])]
    p2 = [int(x) for x in (midpoint[0] - slope[1], midpoint[1] - slope[0])]

    return p1, p2


>>>>>>> norbu_dev
