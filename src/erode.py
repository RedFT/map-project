"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
import cv2
import numpy as np
from constants import *

kernel = np.ones([2, 2], np.uint8)

img = FLOOR_IMAGE.copy()
img = 255 - img
imgnew = cv2.dilate(img, kernel, iterations=1)
imgnew = cv2.erode(img, kernel, iterations=2)
img = 255 - img
imgnew= 255 - imgnew
#imgnew = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

cv2.namedWindow("before", cv2.WINDOW_NORMAL)
cv2.resizeWindow("before", 800, 800)
cv2.namedWindow("after", cv2.WINDOW_NORMAL)
cv2.resizeWindow("after", 800, 800)

cv2.imshow("before", img)
cv2.imshow("after", imgnew)

if cv2.waitKey(0):
    cv2.destroyAllWindows()
