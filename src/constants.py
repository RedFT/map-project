import os
import sys
import cv2
import numpy as np

join_path = os.path.join

input_dir   = join_path(".","input")
output_dir  = join_path(".","output")
data_dir    = join_path(".","data")

#sample_image_filename       = "floorplan4.png"
#sample_database_filename    = "CCNYiNAV_ST6.db"
#sample_image_path       = join_path(input_dir, sample_image_filename)
#sample_database_path    = join_path(data_dir, sample_database_filename)

if len(sys.argv) != 3:
    print "USAGE: python map-project.py [image_file_path] [database_file_path]\n" + \
          "\n" + \
          "  (Ex. python map-project.py ./input/floorplan4.png ./data/CCNYiNAV_ST6.db)" + \
          "\n"
    sys.exit(1)

sample_image_path       = sys.argv[1]
sample_database_path    = sys.argv[2]


map_room_ratio = {}
map_room_area  = {}

FLOOR_IMAGE = cv2.imread(sample_image_path)

REGIONS     = np.ones([
    FLOOR_IMAGE.shape[0], 
    FLOOR_IMAGE.shape[1], 
    3], 
    np.uint8)
REGIONS     = REGIONS * 255

#For video
"""
WRITER      = cv2.VideoWriter(filename = join_path(output_dir, "region_growing.avi"), 
        fourcc=cv2.cv.CV_FOURCC("X", "V", "I", "D"),
        fps=30,
        frameSize=(REGIONS.shape[1], REGIONS.shape[0]))
"""
