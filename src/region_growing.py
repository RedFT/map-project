"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
# Python Standard Library Imports
import time
from random import randrange

# 3rd Party Library Imports
import cv2
import numpy as np

from geometry import *
from drawing import *
from constants import *
from utils import *


class Frontier(object):
    def __init__(self, img):
        super(Frontier, self).__init__()

        self.frontier = set()
        self.frontier_map = np.zeros(img.shape[:2])

    def push(self, point):
        if self.frontier_map[point[1], point[0]] == 0:
            self.frontier_map[point[1], point[0]] = 255
            self.frontier.add(point)

    def pop(self):
        #point = self.frontier.pop(randrange(0, len(self.frontier)))
        point = self.frontier.pop()
        return point

    def __len__(self):
        return len(self.frontier)


def grow_region(img_bw, point, room_regions, room_number=None, color=0):
    """Region growing using MST. (Prim's Algorithm)
    """
    global REGIONS
    global WRITER

    # the frontier is just a list of points to check
    if room_number != None:
        print "Growing Room:", room_number

    frontier = Frontier(img_bw)
    frontier.push(point)

    curr_time = int(round(time.time() * 1000))
    prev_time = int(round(time.time() * 1000))
    area = 0
    list_of_points = []
    display_text = "Finding Known Rooms"
    region_color = (0, 200, 0)
    if room_number >= 2000:
        region_color = (75, 0, 0)
        display_text = "Finding Hallways"
    elif room_number >= 1000:
        region_color = (25, 0, 220)
        display_text = "Finding Unknown Regions"

    while len(frontier):
        area += 1
        # front is the current point to check
        front = frontier.pop()

        list_of_points.append(front[:2])

        mark(img_bw, front, frontier, 
                room_number)
        if room_regions[front[1], front[0]] == color:
            room_regions[front[1], front[0]] = room_number

            REGIONS[front[1], front[0]] = region_color

        """
        curr_time = int(round(time.time() * 1000))
        if curr_time - prev_time > 230.:
            out = REGIONS.copy()
            draw_text(out, display_text, (2, 30), .6, (0, 0, 0))
            cv2.imshow("debug", out)
            WRITER.write(out)
            cv2.waitKey(1)
            prev_time = curr_time
        """

    return (room_regions, (bound_points(list_of_points), area))


def mark(img_bw, point, frontier, room_number):
    """add 'point' to 'region' and update 'frontier'
    """
    x, y = point[:2]
    height, width = img_bw.shape[:2]
    if (x - 1) >= 0:
        if img_bw[y, x-1] != 0:
            frontier.push((x-1,y))

    if (x + 1) < width:
        if img_bw[y, x+1] != 0:
            frontier.push((x+1,y))

    if (y - 1) >= 0:
        if img_bw[y-1, x] != 0:
            frontier.push((x, y-1))

    if (y + 1) < height:
        if img_bw[y+1, x] != 0:
            frontier.push((x, y+1))
