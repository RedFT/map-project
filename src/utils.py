import cv2
from geometry import is_bounded


def push_map(the_map, key, value):
    if key not in the_map:
        the_map[key] = [value]
    else:
        if value not in the_map[key]:
            the_map[key].append(value)

    if value not in the_map:
        the_map[value] = [key]
    else:
        if key not in the_map[value]:
            the_map[value].append(value)

def find_in_region(regions, point):
    """flattens a list of regions into one array
    of points, then searches through the array
    for 'point'
    """
    point_map = [reg for reg in regions]
    for p in point_map:
        if (p[0], p[1]) == (point[0], point[1]):
            return p


def make_black_and_white(img, threshold=0):
    img_gray = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY)
    if threshold:
        return cv2.threshold(img_gray, threshold, 255, cv2.THRESH_BINARY)[1]
    return cv2.threshold(img_gray, 0, 255, cv2.THRESH_OTSU+cv2.THRESH_BINARY)[1]


def look_up_point(point, look_up_table):
    return look_up_table[point[1], point[0]]
