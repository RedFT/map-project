#!/usr/bin/env python2.7
"""
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""


import os

import cv2
from random import randrange
import matplotlib.pyplot as plt
import networkx as nx
import pickle

a_graph = None
with open(os.path.join("./data", "a_graph.dat"), "r") as f:
    a_graph = pickle.load(f)

#x_values = range(0, 2000, 30)
x_values = [randrange(100, 1000) for i in range(0, 100)]
y_values = [randrange(100, 1000) for i in range(0, 100)]
room_list = []
color_list = []
size_list = []
count = 0

G = nx.Graph()
for k, v in a_graph.iteritems():
    if int(k) >= 2000:
        color_list.append((213/255., 188/255., 166/255.))
        size_list.append(5000)
    elif int(k) >= 1000:
        color_list.append((255/255., 208/255., 143/255.))
        size_list.append(1000)
    else:
        color_list.append((217/255., 145/255., 108/255.))
        size_list.append(500)

    key = str(k)
    for val in v:
        value = str(val)
        G.add_edge(key, value)

    room_list.append(key)

nx.draw(G, #pos=new_dic, 
        with_labels=True,
        nodelist=room_list,
        node_size=size_list, 
        node_color=color_list,
        font_size=10,)

plt.draw()
plt.show()
