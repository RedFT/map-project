#!/usr/bin/python2
"""
The MIT License (MIT)

Copyright (c) <2015> <Norbu Tsering>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

"""
# Python Standard Library Imports
import sys
import math
import os
import pickle
from operator import mul # for finding the product of lists/tuples 
from random import randrange
from pprint import pprint
import operator as op
import json

# 3rd Party Library Imports
import sqlite3
import cv2
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

# Project Modules
from src.db_handler import *
from src.geometry import *
from src.drawing import *
from src.region_growing import *
from src.stitch import *
from src.constants import *
from src.utils import *


#temporary fix for door issue #refactor when you get a chance
room_pairs_with_doors = []

def generate_graph(img, cursor, rooms, doors):
    global hallway_room_number
    h, w = img.shape[:2]
    rect = Rect(0, 0, w, h)

    look_up = None
    look_up = get_regions(img, rooms)

    #with open(join_path(data_dir, "lookup_table.dat"), "w") as f:
    with open(join_path(data_dir, "lookup_table.dat"), "r") as f:
        look_up = pickle.load(f)
    #    pickle.dump(look_up, f)


    unknown_regions = []
    room_pairs      = []

    for door in doors:
        look_up, connecting_rooms = get_connecting_rooms(img, door, look_up)
        room_pairs.append(connecting_rooms)

    with open(join_path(data_dir, "lookup_table_with_unknowns.dat"), "w") as f:
        pickle.dump(look_up, f)

    for pair in room_pairs:
        for rm in pair:
            if rm[0] < 1000:
                continue
            if rm[0] not in map_room_ratio:
                map_room_ratio[rm[0]] = rm[2]

    # remove duplicates (kind of)
    map_room_coords = {}
    for pair in room_pairs:
        if len(pair) < 2:
            continue

        r1, r2 = pair[:2]
        push_map(map_room_coords, r1[0], r1[1])
        push_map(map_room_coords, r2[0], r2[1])

    # Generate Graph
    map_graph = {}
    for pair in room_pairs:
        if len(pair) < 2:
            continue

        r1, r2 = pair[:2]
        push_map(map_graph, r1[0], r2[0])
        push_map(map_graph, r2[0], r1[0])

    new_map_graph = {}

    img_bw = make_black_and_white(img.copy(), 254)
    img_bw = 255 - img_bw
    kernel = np.ones([2, 2], np.uint8)
    imgnew = cv2.erode(img_bw, kernel, iterations=1)
    imgnew= 255 - imgnew
    img_bw = imgnew
    cv2.imwrite(join_path(output_dir, "eroded.png"), img_bw)
    unknown_keys = [key for key in map_graph.iterkeys() if key >= 1000]
    for key in unknown_keys:
        # number of connected rooms is greater than 3
        #print key,":",map_room_ratio[key],":",len(map_graph[key])
        if len(map_graph[key]) < 3:
            continue

        # average short_side / average long_side < .6
        if not map_room_ratio[key] < .7:
            continue

        if map_room_area[key] < 1000:
            continue

        look_up, dim_area = grow_region(img_bw, map_room_coords[key][0], 
                look_up, hallway_room_number,
                key)

        if not hallway_room_number in map_room_area:
            map_room_area[hallway_room_number] = dim_area[1]
        else:
            map_room_area[hallway_room_number] +=  dim_area[1]

        # purpose of this line?
        if look_up_point(map_room_coords[key][0], look_up) == key:
            continue

    room_pairs      = []
    for door in doors:
        look_up, connecting_rooms = get_connecting_rooms(img, door, look_up)
        room_pairs.append(connecting_rooms)
        room_pairs_with_doors.append((door, connecting_rooms))

    # remove duplicates (kind of)
    map_room_coords = {}
    for pair in room_pairs:
        if len(pair) < 2:
            continue

        r1, r2 = pair[:2]
        push_map(map_room_coords, r1[0], r1[1])
        push_map(map_room_coords, r2[0], r2[1])

    # Generate Graph
    map_graph = {}
    for pair in room_pairs:
        if len(pair) < 2:
            continue

        r1, r2 = pair[:2]
        if map_room_area[r1[0]] < 150:
            continue

        if map_room_area[r2[0]] < 150:
            continue

        push_map(map_graph, r1[0], r2[0])
        push_map(map_graph, r2[0], r1[0])

    with open(join_path(data_dir, "lookup_table_with_unknowns.dat"), "w") as f:
        pickle.dump(look_up, f)

    with open(join_path(data_dir, "graph.dat"), "w") as f:
        pickle.dump(map_graph, f)

    return map_graph
        

def run(args, cursor):
    img = FLOOR_IMAGE.copy()

    h, w = img.shape[:2]
    rect = Rect(0, 0, w, h)


    # Scale and Translate Rooms and Doors
    new_doors = get_doors(cursor)

    rooms = range(601, 682+1)
    room_coords = []
    for room in rooms:
        room_name = str(room)
        if room == 660:
            room_name = str(room)+"W"
        elif room == 665 or room == 666:
            room_name = str(room)+"M"
        elif room == 627:
            room_name = str(room)+"A"

        point = get_room(cursor, room_name)
        if not point:
            continue

        if is_bounded(rect, point):
            room_coords.append((point[0], point[1], room))

    a_graph = generate_graph(img, cursor, room_coords, new_doors)


    # Mark Rooms and Doors
    for room in room_coords:
        draw_text(img, str(room[2]), 
                room[:2],
                .3,
                (200, 10, 10))

    for door in new_doors:
        # mark door
        draw_point(img, door[:2], (0, 0, 0))

        # draw box around door
        draw_box(img, door[:2], 2, 
                (20, 170, 200))

        draw_text(img, str(door[2]),
                door[:2], .3,
                (125, 10, 150))

        mx = door[3]
        my = door[4]
        angle1 = door[5]
        angle2 = door[6]

        shift_x = mx * math.cos (angle1)
        shift_y = my * math.sin (angle2)

        final_x = door[0] + shift_x
        final_y = door[1] + shift_y

        final_x = int(final_x)
        final_y = int(final_y)

        draw_box(img, (final_x, door[1]), 1, 
                (0, 0, 200))
        draw_box(img, (door[0], final_y), 1, 
                (0, 0, 200))
        draw_box(img, (final_x, final_y), 1, 
                (0, 0, 200))


    cv2.imwrite(join_path(output_dir, "out.png"), img)


if __name__ == "__main__":
    toggle_drawing()
    cv2.namedWindow("debug", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("debug", 800, 600)

    conn = sqlite3.connect(sample_database_path)
    cursor = conn.cursor()

    run(sys.argv, cursor)
    """
    REGIONS = cv2.imread(join_path(output_dir, "regions_plain.png"))
    """

    h, w = REGIONS.shape[:2]

    cv2.imwrite(join_path(output_dir, "regions_plain.png"), REGIONS)
    im_bw = make_black_and_white(REGIONS)

    cv2.imwrite(join_path(output_dir, "regions_plain_bw_orig.png"), im_bw)

    contours     = cv2.findContours(im_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]
    contours_all = cv2.findContours(im_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)[0]
    #contours = cv2.findContours(im_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_TC89_L1)[0]
    #contours = cv2.findContours(im_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_TC89_KCOS)[0]

    # Remove Biggest Contour
    biggest_contour = 0
    for i, c in enumerate(contours[:]):
        area = cv2.contourArea(c)
        if area > cv2.contourArea(contours[i]):
            biggest_contour = i
    contours.pop(i)

    # Remove contours that are too small (area < 100px)
    rel_contours = []
    for c in contours:
        area = cv2.contourArea(c)
        if area > 100:
            rel_contours.append(c)
    """
    rel_contours = contours
    """

    outlines = np.zeros([h, w, 3], np.uint8)

    print len(rel_contours), "contours detected"

    # Load look up table
    look_up = None
    with open(join_path(data_dir, "lookup_table_with_unknowns.dat"), "r") as f:
        look_up = pickle.load(f)


    room_contour_pairs = []
    for i in range(0, len(rel_contours)):
        simple_points = []
        points = [x[0] for x in [x for x in rel_contours[i]]]
        #print "Rect:",bound_points(points, 5)
        bbox = bound_points(points, 5)

        # center of mass; not currently used;
        M = cv2.moments(rel_contours[i])
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

        midpoint = 0
        slope = 0
        point_1 = 0
        point_2 = 0

        for j in range(1, len(points)):
            midpoint = (points[j] - points[j-1]) / 2 + points[j-1]
            slope = (points[j] - points[j-1])
            slope = slope / math.sqrt((slope[0] ** 2 + slope[1] ** 2))
            unit_p1 = [int(x) for x in (midpoint[0] + slope[1] * 10, midpoint[1] + slope[0] * 10)]
            unit_p2 = [int(x) for x in (midpoint[0] - slope[1] * 10, midpoint[1] - slope[0] * 10)]

            slope *= 100
            #print "Unit Slope: ", slope
            p1 = [int(x) for x in (midpoint[0] + slope[1], midpoint[1] + slope[0])]
            p2 = [int(x) for x in (midpoint[0] - slope[1], midpoint[1] - slope[0])]

            slope = (points[j] - points[j-1])
            
            # break if we found a segment long enough to test
            length = math.sqrt((slope[0] ** 2 + slope[1] ** 2))
            if length > 3:
                break

        point_1, point_2 = line_box_intersection((p1, p2), bbox)

        intersection_points = []
        for j in range(1, len(points)):
            intersection = line_intersection((point_1, point_2), (points[j], points[j-1]))
            if intersection:
                #check if the intersection lies on both lines
                real_intersection = True
                for line in [(point_1, point_2), (points[j], points[j-1])]:
                    pa = (get_magnitude(line[0], intersection))
                    pb = (get_magnitude(line[1], intersection))
                    pc = (get_magnitude(line[0], line[1]))
                    #print pa, pb, pc
                    if (abs(pa + pb - pc) > .001):
                        # intersection is not on one of the lines
                        real_intersection = False
                if real_intersection == True:
                    intersection_points.append(intersection)

        inner_point = None
        intersection_points = sorted(intersection_points, key=op.itemgetter(0, 1))

        for test_point in [unit_p1, unit_p2]:
            for j in range(1, len(intersection_points)):
                line = (intersection_points[j-1], intersection_points[j])
                pa = (get_magnitude(line[0], test_point))
                pb = (get_magnitude(line[1], test_point))
                pc = (get_magnitude(line[0], line[1]))

                not_inside = abs((pa + pb) - pc) > .001

                if not_inside:
                    continue

                if (j % 2 != 0):
                    inner_point = test_point

        lu_h, lu_w, = look_up.shape
        room = None
        try:
            if inner_point:
                room = look_up[inner_point[1], inner_point[0]]
        except IndexError:
            print "Index Error"

        if room:
            cv2.drawContours(outlines, rel_contours, i, (150, 150, 150))
            """
            cv2.rectangle(outlines, 
                    (bbox[0], bbox[1]),
                    (bbox[0] + bbox[2], bbox[1] + bbox[3]),
                    (255, 0, 0))
            """
            #draw_box(outlines, midpoint, 2, (0, 0, 255))
            cv2.line(outlines, tuple([int(x) for x in point_1]), tuple([int(x) for x in point_2]), (0, 0, 255))

            for ip in intersection_points:
                draw_box(outlines, [int(x) for x in ip], 2, (0, 255, 0))

            if inner_point:
                draw_box(outlines, inner_point, 3, (0, 255, 255))

            draw_text(outlines, str(room), (cx - 15, cy),
                    .3,
                    (200, 10, 10))

            draw_text(REGIONS, str(room), (cx - 15, cy),
                    .3,
                    (200, 10, 10))

            room_contour_pairs.append((rel_contours[i], room))

    clean_image = np.zeros([h, w, 3], np.uint8)
    floor = {}
    hallway = 0

    for contour, room in room_contour_pairs:
        contour_points = []

        
        for i in range(1, len(contour)):
            if i == 1:
                p0 = contour[i-2][0]
                p1 = contour[i-1][0]
                p2 = contour[i][0]
            else:
                p0 = np.array(contour_points[-1])
                p1 = contour[i-1][0]
                p2 = contour[i][0]
                #print p0, p1, p2

            if np.array_equal(p1, p0):
                continue

            if np.sqrt(abs((p2-p1).dot(p2-p1))) - np.sqrt(2) < 0.01:
                p1 = p1 + ((p1 - p0)/np.sqrt(abs((p1-p0).dot(p1-p0))))

            contour_points.append([int(p1[0]), int(p1[1])])
        contour_points.append([int(contour[-1][0][0]), int(contour[-1][0][1])])
        contour_points.append([int(contour[0][0][0]), int(contour[0][0][1])])

        if room >= 2000:
            floor["h" + str(room + hallway)] = [contour_points]
            hallway += 1
        elif room >= 1000:
            floor["u" + str(room)] = [contour_points]
        else:
            floor["r" + str(room)] = [contour_points]

        #Draw contours to test accuracy
        for i in range(1, len(contour_points)):
            cv2.line(clean_image, tuple(contour_points[i-1]), tuple(contour_points[i]), (255, 255, 255))
    cv2.imwrite(join_path(output_dir, "clean_image.png"), clean_image)



    # Mark doors in REGION
    hallway = 0
    for pair in room_pairs_with_doors:
        for room in pair[1]:
            room_string = ""
            if room[0] >= 2000:
                room_string = "h" + str(room[0] + hallway)
                hallway += 1
            elif room[0] >= 1000:
                room_string = "u" + str(room[0])
            else:
                room_string = "r" + str(room[0])
            try:
                print "Trying room: " + room_string,
                floor[room_string].append([pair[0][0], pair[0][1]])
            except KeyError:
                print "invalid room: " + room_string
            print


    with open("floor.json", "w") as f:
        json.dump(floor, f)

    #cv2.imshow("debug", REGIONS)
    #cv2.waitKey(0)

    cv2.imwrite(join_path(output_dir, "regions.png"), REGIONS)
    cv2.imwrite(join_path(output_dir, "regions_bw.png"), make_black_and_white(REGIONS))
    cv2.imwrite(join_path(output_dir, "outlines.png"), outlines)

    """
    if not table_exists(cursor, "room"):
        print "Table doesn't exist"
    else:
        print "Table exists already"
    """

    conn.close()
    cv2.destroyAllWindows()
